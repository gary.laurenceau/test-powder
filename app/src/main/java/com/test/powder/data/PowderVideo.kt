package com.test.powder.data

data class PowderVideo(
    val id: Int,
    val video_url: String,
    val title: String,
    val game: String,
    val author: String
)