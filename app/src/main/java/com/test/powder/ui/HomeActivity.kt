package com.test.powder.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.viewpager2.widget.ViewPager2
import androidx.viewpager2.widget.ViewPager2.ORIENTATION_HORIZONTAL
import com.google.android.material.snackbar.Snackbar
import com.test.powder.R
import com.test.powder.data.PowderVideo
import com.test.powder.ui.adapter.VideoAdapter
import kotlinx.android.synthetic.main.activity_main.*

class HomeActivity: AppCompatActivity() {

    // Dependency
    private val homeViewModel: HomeViewModel by lazy { ViewModelProvider(this).get(HomeViewModel::class.java) }
    private val adapter: VideoAdapter by lazy { VideoAdapter(this) }

    // Lifecycle

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setupViewPager()

        // Observe data
        homeViewModel.observeVideoData().observe(this) { onVideoLoaded(it) }
        homeViewModel.observeError().observe(this) { onError(it) }

        // Load videos
        homeViewModel.fetchVideos()
    }

    override fun onPause() {
        super.onPause()
        adapter.onPauseVideo()
    }

    override fun onDestroy() {
        super.onDestroy()
        adapter.onRelease()
    }

    // Data callbacks

    private fun onVideoLoaded(videos: List<PowderVideo>) {
        // Quick trick to create infinite loop for ViewPager
        val data = mutableListOf<PowderVideo>()
        data.addAll(videos)
        data.add(videos.first())
        data.add(0, videos.last())

        // Infinite loop listener
        with(viewPager) {
            registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
                override fun onPageScrollStateChanged(state: Int) {
                    super.onPageScrollStateChanged(state)
                    if (state == ViewPager2.SCROLL_STATE_IDLE || state == ViewPager2.SCROLL_STATE_DRAGGING) {
                        if (currentItem == 0)
                            setCurrentItem(data.size - 2, false)
                        else if (currentItem == data.size -1)
                            setCurrentItem(1, false)
                    }
                }
            })
        }

        // Set data for adapter
        adapter.list.clear()
        adapter.list.addAll(data)
        adapter.notifyDataSetChanged()
        viewPager.setCurrentItem(1, false)
    }

    private fun onError(exception: Throwable) {
        val errorMessage = exception.message ?: getString(R.string.unexpected_error)
        Snackbar.make(container, errorMessage, Snackbar.LENGTH_LONG).show()
    }

    // UI

    private fun setupViewPager() {
        viewPager.adapter = adapter
        viewPager.orientation = ORIENTATION_HORIZONTAL
        viewPager.offscreenPageLimit = 4
        viewPager.registerOnPageChangeCallback(object: ViewPager2.OnPageChangeCallback() {
            override fun onPageSelected(position: Int) {
                adapter.onVideoSelected(position)
            }
        })
    }
}