package com.test.powder.ui.adapter

import android.net.Uri
import android.view.View
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import com.google.android.exoplayer2.ExoPlayerFactory
import com.google.android.exoplayer2.Player
import com.google.android.exoplayer2.SimpleExoPlayer
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory
import com.google.android.exoplayer2.extractor.ExtractorsFactory
import com.google.android.exoplayer2.source.ExtractorMediaSource
import com.google.android.exoplayer2.source.MediaSource
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector
import com.google.android.exoplayer2.trackselection.TrackSelector
import com.google.android.exoplayer2.upstream.BandwidthMeter
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory
import com.test.powder.data.PowderVideo
import kotlinx.android.synthetic.main.item_video.view.*

class VideoViewHolder(
    view: View,
): RecyclerView.ViewHolder(view), VideoAdapter.VideoControlListener {

    // Player
    private var powderVideo: PowderVideo? = null
    private val dataSourceFactory: DefaultHttpDataSourceFactory by lazy { DefaultHttpDataSourceFactory("exoplayer_video") }
    private val extractorsFactory: ExtractorsFactory by lazy { DefaultExtractorsFactory() }

    init {
        // Setup bandwidth
        val bandwidthMeter: BandwidthMeter = DefaultBandwidthMeter()
        val trackSelector: TrackSelector = DefaultTrackSelector(AdaptiveTrackSelection.Factory(bandwidthMeter))

        view.playerView.player = ExoPlayerFactory.newSimpleInstance(view.context, trackSelector)
        (itemView.playerView.player as SimpleExoPlayer).apply {
            addListener(object : Player.DefaultEventListener() {
                override fun onPlayerStateChanged(playWhenReady: Boolean,playbackState: Int) {
                    when (playbackState) {
                        Player.STATE_IDLE -> itemView.progressBar.isVisible = true
                        Player.STATE_BUFFERING -> itemView.progressBar.isVisible = true
                        Player.STATE_READY -> itemView.progressBar.isVisible = false
                        Player.STATE_ENDED -> itemView.progressBar.isVisible = false
                    }
                }
            })
        }
    }

    fun bind(newVideo: PowderVideo) {
        // Set source
        powderVideo = newVideo
        val mediaSource: MediaSource = ExtractorMediaSource(
            Uri.parse(newVideo.video_url),
            dataSourceFactory,
            extractorsFactory,
            null,
            null
        )
        // Set video source
        (itemView.playerView.player as SimpleExoPlayer).prepare(mediaSource)
        // Set position
        itemView.playerView.player.seekTo(0)
        // Don' auto start video
        itemView.playerView.player.playWhenReady = false
        // Loop when video is finished.
        itemView.playerView.player.repeatMode = Player.REPEAT_MODE_ALL

        // Title
        itemView.title.text = newVideo.title
    }

    override fun getVideo(): PowderVideo? {
        return powderVideo
    }

    override fun playVideo() {
        itemView.playerView.player.playWhenReady = true
    }

    override fun pauseVideo() {
        itemView.playerView.player.playWhenReady = false
    }

    override fun release() {
        itemView.playerView.player.stop()
        itemView.playerView.player.release()
    }
}