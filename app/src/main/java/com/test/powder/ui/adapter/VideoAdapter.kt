package com.test.powder.ui.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.google.android.exoplayer2.video.VideoListener
import com.test.powder.R
import com.test.powder.data.PowderVideo

class VideoAdapter(private val context: Context): RecyclerView.Adapter<VideoViewHolder>() {

    val list = mutableListOf<PowderVideo>()
    private val videoListeners = mutableListOf<VideoControlListener>()
    private val inflater: LayoutInflater by lazy { LayoutInflater.from(context) }
    private var currentPlayingItem: PowderVideo? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VideoViewHolder {
        return VideoViewHolder(inflater.inflate(R.layout.item_video, parent, false)).apply {
            videoListeners.add(this)
        }
    }

    override fun onBindViewHolder(holder: VideoViewHolder, position: Int) {
        val videoToDisplay = list[position]
        holder.bind(videoToDisplay)
    }

    override fun getItemCount(): Int = list.size

    fun onVideoSelected(position: Int) {
        currentPlayingItem = list[position]
        videoListeners.forEach { it.pauseVideo() }
    }

    fun onPauseVideo() {
        videoListeners.forEach { it.pauseVideo() }
    }

    fun onRelease() {
        videoListeners.forEach { it.release() }
    }

    interface VideoControlListener {
        fun getVideo(): PowderVideo?
        fun playVideo()
        fun pauseVideo()
        fun release()
    }
}