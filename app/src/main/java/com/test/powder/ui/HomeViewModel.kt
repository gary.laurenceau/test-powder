package com.test.powder.ui

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.test.powder.data.PowderVideo
import com.test.powder.domain.PowderClient
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class HomeViewModel: ViewModel() {

    // Use DI instead of direct instance
    private val powderClient: PowderClient by lazy { PowderClient() }

    private val onVideoData = MutableLiveData<List<PowderVideo>>()
    private val onError = MutableLiveData<Throwable>()

    fun observeVideoData(): LiveData<List<PowderVideo>> = onVideoData
    fun observeError(): LiveData<Throwable> = onError

    fun fetchVideos() {
        powderClient.getVideos()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                onVideoData.value = it
            }, {
                onError.value = it
            })
    }

}