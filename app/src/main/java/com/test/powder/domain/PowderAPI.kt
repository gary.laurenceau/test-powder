package com.test.powder.domain

import com.test.powder.data.PowderVideo
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

interface PowderAPI {

    @GET("v0/b/powder-c9282.appspot.com/o/test%2Fstatic_feed.json")
    fun getVideos(
        @Query("alt") alt: String = "media",
        @Query("token") token: String = "c5bbde3a-129b-449e-a79e-d2a0ccffbd0f"
    ): Observable<List<PowderVideo>>


}