package com.test.powder.domain

import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import com.test.powder.data.PowderVideo
import io.reactivex.Observable
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class PowderClient {

    companion object {
        const val BASE_API = "https://firebasestorage.googleapis.com/"
    }

    private val client: PowderAPI

    init {
        val retrofit = Retrofit.Builder()
            .baseUrl(BASE_API)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()

        client = retrofit.create(PowderAPI::class.java)
    }

    fun getVideos(): Observable<List<PowderVideo>> {
        return client.getVideos()
    }

}